import asyncio
import re
from concurrent.futures import ThreadPoolExecutor

import discord
from discord.ext import commands

from cogs.utils import checks
from cogs.utils.paginator import ListPaginator
from cogs.utils.storage import RedisCollection


class AriaFilter:
    def __init__(self, liara):
        self.liara = liara
        self.filters = RedisCollection(self.liara.redis, 'aria.filters')
        self.tests = ['Hello, world!', 'How\'s it going?', 'Good, how abut you?', 'hello world', 'hows it going',
                      'good hbu']
        self.create_gist = liara.get_cog('Core').create_gist
        self.pool = ThreadPoolExecutor(max_workers=4, thread_name_prefix='aria-filters')

    async def run_regex_async(self, regex, text):
        return asyncio.wrap_future(self.pool.submit(regex.search, text))

    async def on_message(self, message):
        if not isinstance(message.channel, discord.TextChannel):
            return
        if not message.content:
            return
        if message.author == message.guild.me:
            return
        if message.author.permissions_in(message.channel).manage_messages:
            return
        filters = await self.filters.get(message.guild.id, [])
        for regex in filters:
            if await self.run_regex_async(regex, message.content):
                await message.delete()
                await message.channel.send('{}, your message has been removed in accordance with this server\'s '
                                           'message filters.'.format(message.author.mention), delete_after=10)

    @commands.group('filters', invoke_without_command=True, aliases=['filter'])
    @checks.mod_or_permissions(manage_messages=True)
    async def filters_cmd(self, ctx):
        """Adds / removes / lists regex filters.
        
        Test your regexes at https://regex101.com
        
        The regexes are evaluated case-insensitive, on a first-match basis.
        """
        await self.liara.send_command_help(ctx)

    @filters_cmd.command()
    @checks.mod_or_permissions(manage_messages=True)
    async def add(self, ctx, *, _filter: str):
        """Adds a regex filter."""
        regex = re.compile(_filter, flags=re.IGNORECASE)
        for test in self.tests:
            if await self.run_regex_async(regex, test):
                return await ctx.send('Sorry, that regex is too broad, it would match regular conversation.')
        filters = await self.filters.get(ctx.guild.id, [])
        if len(filters) >= 25:
            return await ctx.send('You have too many filters. Try optimizing your existing regexes instead.')
        if regex in filters:
            return await ctx.send('That regex is already added.')
        filters.append(regex)
        await self.filters.set(ctx.guild.id, filters)
        await ctx.send('Regex added. You now have {} filter{}.'.format(len(filters), 's' if len(filters) != 1 else ''))

    @filters_cmd.command()
    @checks.mod_or_permissions(manage_messages=True)
    async def addinvite(self, ctx):
        """Adds a filter for Discord invites."""
        regex = re.compile(r'discord(?:app)?\.(?:gg|com/invite)/(?:[0-9A-Za-z]+)', flags=re.IGNORECASE)
        filters = await self.filters.get(ctx.guild.id, [])
        if regex in filters:
            return await ctx.send('The invite filter is already added.')
        filters.append(regex)
        await self.filters.set(ctx.guild.id, filters)
        await ctx.send('Invite filter added. You now have {} filter{}.'
                       .format(len(filters), 's' if len(filters) != 1 else ''))

    @filters_cmd.command('list')
    @checks.mod_or_permissions(manage_messages=True)
    async def _list(self, ctx):
        """Lists all regex filters."""
        filters = await self.filters.get(ctx.guild.id)
        filters = [f'`{x.pattern}`' for x in filters]
        paginator = ListPaginator(ctx, filters)
        await paginator.begin()

    @filters_cmd.command(aliases=['delete'])
    @checks.mod_or_permissions(manage_messages=True)
    async def remove(self, ctx, index: int):
        """Removes a regex filter using its index number."""
        try:
            filters = await self.filters.get(ctx.guild.id, [])
            regex = filters.pop(index-1)
            await self.filters.set(ctx.guild.id, filters)
        except IndexError:
            return await ctx.send('That index location is invalid.')
        await ctx.send('Regex `{}` sucessfully removed.'.format(regex.pattern))


def setup(liara):
    liara.add_cog(AriaFilter(liara))
