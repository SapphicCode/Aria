import discord
from discord.ext import commands

from cogs.utils import checks


class AriaMeta:
    def __init__(self, liara):
        self.liara = liara
        self.db = None
        self.liara.loop.create_task(self._init())

    async def _init(self):
        cog = self.liara.get_cog('DB')
        await cog.ready.wait()
        self.db = cog.psql

    @commands.command()
    async def feedback(self, ctx, *, message: str):
        """Submits feedback.
        - message: The feedback message to submit
        """
        count = await self.db.fetchval('SELECT count(*) FROM aria.feedback WHERE author_id = $1', ctx.author.id)
        if count >= 10:
            return await ctx.send('Sorry, you can only have 10 pieces of feedback open at once. Please wait for your '
                                  'current feedback to be read before submitting more.')
        await self.db.execute('INSERT INTO aria.feedback (author_id, message) VALUES ($1, $2)',
                              ctx.author.id, message)
        await ctx.send(content='Feedback submitted. Thanks for helping to improve Aria for everyone '
                               '\N{SLIGHTLY SMILING FACE}.')

    @commands.command()
    @commands.guild_only()
    @checks.mod_or_permissions(manage_messages=True)
    async def officialserver(self, ctx):
        """Sends an invite link to the official server."""
        official_server = self.liara.get_guild(345867318024470530)
        if official_server is None:
            return await ctx.send('Internal error, please try again later.')
        if official_server.unavailable:
            return await ctx.send('The official server seems to be unavailable right now, please try again later.')
        reason = 'Requested join from guild {0} ({0.id}) by user {1} ({1.id})'.format(ctx.guild, ctx.author)
        invite = await official_server.text_channels[0].create_invite(max_age=3600, max_uses=8, reason=reason)
        await ctx.send('Invite: {}'.format(invite.url))

    @commands.command()
    @checks.is_owner()
    async def dm(self, ctx, uid: int, *, message: str):
        user = self.liara.get_user(uid)
        if user is None:
            return await ctx.send('No user with that ID found.')
        try:
            await user.send(message + '\n\n*This is a message sent because you have previously reported feedback. '
                            'Replies will not be received.*')
            await ctx.send('Message sent.')
        except discord.Forbidden:
            await ctx.send('Unable to send DM.')


def setup(liara):
    liara.add_cog(AriaMeta(liara))
