import asyncio
import datetime
from typing import Optional

import asyncpg
import discord
from discord.ext import commands

from cogs.utils import checks


def remove_markdown(instr):
    return instr.replace('\\', '\\\\').replace('*', '\\*').replace('`', '\\`').replace('_', '\\_').replace('~', '\\~')


class AriaModlog:
    def __init__(self, liara):
        self.liara = liara
        self.last_ban = {}
        self.db = None
        self.liara.loop.create_task(self.init())

    async def init(self):
        cog = self.liara.get_cog('DB')
        await cog.ready.wait()
        self.db = cog.psql

    async def get_modlog_channel(self, guild_id) -> Optional[discord.TextChannel]:
        channel_id = await self.db.fetchval(
            'SELECT modlog_channel_id FROM aria.guild_options WHERE guild = $1', guild_id
        )
        if not channel_id:
            return
        return self.liara.get_channel(channel_id)

    async def set_modlog_channel(self, guild_id, channel_id):
        q = """
            INSERT INTO aria.guild_options (guild, modlog_channel_id) VALUES ($1, $2)
            ON CONFLICT (guild) DO UPDATE SET modlog_channel_id = EXCLUDED.modlog_channel_id
            """
        await self.db.execute(q, guild_id, channel_id)

    async def get_user(self, uid: int) -> discord.User:
        user = None  # database fetch
        if user is not None:
            # noinspection PyProtectedMember
            return discord.User(state=self.liara._connection, data=user)  # I'm sorry Danny

        user = self.liara.get_user(uid)
        if user is not None:
            return user

        try:
            user = await self.liara.get_user_info(uid)
        except discord.NotFound:
            user = None
        if user is not None:  # intentionally leaving this at the end so we can add more methods after this one
            return user

    @staticmethod
    async def react_or_messages(ctx, reaction, message):
        if ctx.guild.me.permissions_in(ctx.channel).add_reactions:
            return await ctx.message.add_reaction(reaction)
        await ctx.send(message)

    @staticmethod
    async def get_latest_case(conn, guild: int) -> Optional[asyncpg.Record]:
        return await conn.fetchrow('SELECT * FROM aria.modlog WHERE guild = $1 ORDER BY "case" DESC LIMIT 1', guild)

    @staticmethod
    async def get_case(conn, guild: int, case: int) -> Optional[asyncpg.Record]:
        return await conn.fetchrow('SELECT * FROM aria.modlog WHERE guild = $1 AND "case" = $2', guild, case)

    async def new_case(self, guild, action, user, moderator=None, reason=None):
        channel = await self.get_modlog_channel(guild.id)
        if channel is None:
            return
        async with self.db.acquire() as c:
            _last = await self.get_latest_case(c, guild.id)
            if _last is None:
                case_num = 1
            else:
                case_num = _last['case'] + 1

            softban = (
                _last and _last['action'] == 'Ban' and action == 'Unban' and _last['user'] == user.id and
                _last['moderator'] == moderator and
                datetime.datetime.utcnow() - _last['timestamp'] < datetime.timedelta(minutes=5)
            )

            if softban:
                case_num -= 1
                await c.execute('UPDATE aria.modlog SET action = \'Softban\' WHERE guild = $1 AND "case" = $2',
                                guild.id, case_num)
            else:
                await c.execute('INSERT INTO aria.modlog (guild, "case", action, moderator, reason, "user") '
                                'VALUES ($1, $2, $3, $4, $5, $6)',
                                guild.id, case_num, action, moderator, reason, user.id)
            await self.format_case(c, guild, case_num)

    async def format_case(self, conn, guild, case_num):
        channel = await self.get_modlog_channel(guild.id)
        if channel is None:
            return

        case = await self.get_case(conn, guild.id, case_num)
        if case is None:
            return

        case = dict(case)

        message = None
        if case['message'] is not None:
            try:
                message = await channel.get_message(case['message'])
            except discord.NotFound or discord.Forbidden:
                pass

        action = case['action']
        user = await self.get_user(case['user'])
        username = remove_markdown(str(user))
        prefix = self.liara.command_prefix[0]
        reason = case.get('reason')

        text = f'**{action}** | Case {case_num}\n**User:** {username} ({user.id})\n'
        if reason is None:
            reason = f'*Responsible moderator, please use `{prefix}reason {case_num} <reason>`.*'
        text += '**Reason:** {}\n'.format(reason.strip())  # thanks b1nzy
        if case['moderator'] is not None:
            moderator = await self.get_user(case['moderator'])
            moderator_name = remove_markdown(str(moderator))
            text += f'**Responsible moderator:** {moderator_name} ({moderator.id})\n'
        text = text.strip()

        if message is None:
            try:
                message = await channel.send(text)
            except discord.HTTPException:
                return
            await conn.execute('UPDATE aria.modlog SET message = $1 WHERE guild = $2 AND "case" = $3',
                               message.id, guild.id, case_num)
        else:
            await message.edit(content=text)

    @staticmethod
    async def get_log(guild, action):
        _logs = []
        try:
            async for log in guild.audit_logs(action=action):
                if log.created_at > datetime.datetime.utcnow()-datetime.timedelta(seconds=15):
                    _logs.append(log)
        except discord.Forbidden:
            return
        if _logs:
            return _logs[0]
        else:
            return None

    async def on_member_ban(self, guild, user):
        await asyncio.sleep(1)
        ban_event = await self.get_log(guild, discord.AuditLogAction.ban)
        if ban_event is None:
            return await self.new_case(guild, 'Ban', user)
        if ban_event.reason == 'Global ban' and ban_event.user.id == self.liara.user.id:
            return
        moderator_id = ban_event.user.id
        reason = ban_event.reason
        await self.new_case(guild, 'Ban', user, moderator=moderator_id, reason=reason)

    async def on_member_unban(self, guild, user):
        await asyncio.sleep(1)
        unban_event = await self.get_log(guild, discord.AuditLogAction.unban)
        if unban_event is None:
            return await self.new_case(guild, 'Unban', user)
        if unban_event.reason == 'Global ban' and unban_event.user.id == self.liara.user.id:
            return
        moderator_id = unban_event.user.id
        reason = unban_event.reason
        await self.new_case(guild, 'Unban', user, moderator=moderator_id, reason=reason)

    async def on_member_remove(self, member):
        await asyncio.sleep(1)
        kick_event = await self.get_log(member.guild, discord.AuditLogAction.kick)
        if kick_event is None:
            return
        if member.id != kick_event.target.id:
            return
        await self.new_case(member.guild, 'Kick', member, moderator=kick_event.user.id, reason=kick_event.reason)

    @commands.command()
    @checks.admin_or_permissions(manage_channel=True)
    async def setml(self, ctx, channel: discord.TextChannel):
        """Sets the mod log to the specified channel.
        - channel: The channel to send ban messages to
        """
        if not ctx.guild.me.permissions_in(channel).send_messages:
            return await ctx.send('I don\'t have permission to send messages to that channel.')
        if not ctx.guild.me.permissions_in(channel).view_audit_log:
            await ctx.send('I do not have permission to view the audit log, so I can\'t automatically attach '
                           'moderators and their reasons with affected users.')
        await self.set_modlog_channel(ctx.guild.id, channel.id)
        await ctx.send('Mod log channel set to {}.'.format(channel.mention))

    @commands.command()
    @checks.admin_or_permissions(manage_channel=True)
    async def disableml(self, ctx):
        """Disables the mod log for the current server."""
        channel = await self.get_modlog_channel(ctx.guild.id)
        if channel:
            await self.set_modlog_channel(ctx.guild.id, None)
            await ctx.send('Mod log disabled.')
        else:
            await ctx.send('The mod log was already disabled.')

    @commands.command()
    @commands.guild_only()
    @checks.admin_or_permissions(manage_channel=True)
    async def resetml(self, ctx):
        """Resets the mod log and its cases for the current server."""
        async with self.db.acquire() as c:
            await c.execute('DELETE FROM aria.modlog WHERE guild = $1', ctx.guild.id)
        await ctx.send('Database deleted.')

    @commands.command()
    @commands.guild_only()
    @checks.mod_or_permissions(kick_members=True)
    async def reason(self, ctx, case: int, *, reason: str):
        """Sets a mod action reason.
        - case: Numeric case identifier to target
        - reason: The reason for the action
        """
        async with self.db.acquire() as c:
            num = case
            case = await self.get_case(c, ctx.guild.id, num)
            if case is None:
                return await ctx.send('That case number appears to be invalid.')
            await c.execute('UPDATE aria.modlog SET moderator = $1, reason = $2 WHERE guild = $3 AND "case" = $4',
                            ctx.author.id, reason, ctx.guild.id, num)
            await self.format_case(c, ctx.guild, num)
            await self.react_or_messages(ctx, '\N{WHITE HEAVY CHECK MARK}', 'Case updated.')

    @commands.command()
    @commands.guild_only()
    @checks.mod_or_permissions(kick_members=True)
    async def refresh(self, ctx, case: int):
        """Refreshes a case.

        This updates things like moderator and user names.
        """
        async with self.db.acquire() as c:
            _case = await self.get_case(c, ctx.guild.id, case)
            if _case is None:
                return await self.react_or_messages(ctx, '\N{NEGATIVE SQUARED CROSS MARK}', 'Case not found.')
            await self.format_case(c, ctx.guild, case)
        await self.react_or_messages(ctx, '\N{WHITE HEAVY CHECK MARK}', 'Case refreshed.')


def setup(liara):
    liara.add_cog(AriaModlog(liara))
