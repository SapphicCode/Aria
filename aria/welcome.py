import discord
from discord.ext import commands

from cogs.utils import checks


class AriaWelcome:
    def __init__(self, liara):
        self.liara = liara
        self.db = None
        self.liara.loop.create_task(self._init())

    async def _init(self):
        cog = self.liara.get_cog('DB')
        await cog.ready.wait()
        self.db = cog.psql

    async def create_row(self, guild_id):
        await self.db.execute('INSERT INTO aria.welcome (guild) VALUES ($1) ON CONFLICT DO NOTHING', guild_id)

    async def on_member_join(self, member):
        if self.db is None:
            return
        row = await self.db.fetchrow('SELECT * FROM aria.welcome WHERE guild = $1', member.guild.id)
        if not row or not row['channel'] or not row['message']:
            return
        channel = self.liara.get_channel(row['channel'])
        if not channel:
            return await self.db.execute('UPDATE aria.welcome SET channel = NULL WHERE guild = $1', member.guild.id)
        message = row['message']

        # mods
        message = message.replace('$name$', member.name)
        message = message.replace('$mention$', member.mention)
        message = message.replace('$user$', str(member))
        message = message.replace('$server$', member.guild.name)
        message = message.replace('$channel$', channel.name)
        message = message.replace('$cmention$', channel.mention)

        try:
            if row['embed']:
                return await channel.send(embed=discord.Embed(description=message))
            await channel.send(message)
        except discord.Forbidden:
            return await self.db.execute('UPDATE aria.welcome SET channel = NULL WHERE guild = $1', member.guild.id)

    @commands.group(aliases=['welcomeset'], invoke_without_command=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def wset(self, ctx):
        """Sets welcome settings."""
        await self.liara.send_command_help(ctx)

    @wset.command()
    @checks.mod_or_permissions(manage_channels=True)
    async def channel(self, ctx, channel: discord.TextChannel=None):
        """
        Sets the welcome channel.

        - channel: the channel to set as the welcome channel (leave blank to disable)
        """
        channel = channel.id if channel else None
        await self.create_row(ctx.guild.id)
        await self.db.execute('UPDATE aria.welcome SET channel = $2 WHERE guild = $1', ctx.guild.id, channel)
        row = await self.db.fetchrow('SELECT * FROM aria.welcome WHERE guild = $1', ctx.guild.id)

        if channel:
            if not row['message']:
                return await ctx.send('Channel set. Please also remember to set your welcome message.')
            await ctx.send('Channel set.')
        else:
            await ctx.send('Channel cleared. If you didn\'t intend to do this, refer to the help menu below:')
            await self.liara.send_command_help(ctx)

    @wset.command()
    @checks.mod_or_permissions(manage_channels=True)
    async def message(self, ctx, *, text):
        """
        Sets the welcome message.

        - text: the text for the welcome message

        Note that the text supports variables as listed below:
        - $name$:     Replaced with the user's name
        - $mention$:  Replaced with a mention to the user
        - $user$:     Replaced with the user's username#discrim combo
        - $server$:   Replaced with the server's name
        - $channel$:  Replaced with the channel's name
        - $cmention$: Replaced with the channel's mention
        """
        text = text.strip()
        if not text:
            return await ctx.send('No text was given.')

        await self.create_row(ctx.guild.id)
        await self.db.execute('UPDATE aria.welcome SET message = $2 WHERE guild = $1', ctx.guild.id, text)
        row = await self.db.fetchrow('SELECT * FROM aria.welcome WHERE guild = $1', ctx.guild.id)

        if not row['channel']:
            return await ctx.send('Welcome message set. Please remember to also set your welcome channel.')
        await ctx.send('Welcome message set.')

    @wset.command()
    @checks.mod_or_permissions(manage_channels=True)
    async def embed(self, ctx, state: bool):
        """
        Sets whether to use embeds for welcome messages or not.

        - state: on/off/yes/no/true/false
        """
        await self.create_row(ctx.guild.id)
        await self.db.execute('UPDATE aria.welcome SET embed = $2 WHERE guild = $1', ctx.guild.id, state)
        await ctx.send('Setting saved.')
