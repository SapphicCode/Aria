import argparse
import asyncio

import asyncpg
import discord
from discord.ext import commands

from cogs.utils import checks


class DiscordArgparseError(Exception):
    pass


class DiscordArgparseMessage(DiscordArgparseError):
    pass


class DiscordFriendlyArgparse(argparse.ArgumentParser):
    def _print_message(self, message, file=None):
        raise DiscordArgparseMessage(f'```\n{message}\n```')

    def error(self, message):
        raise DiscordArgparseError(f'```\n{self.format_usage()}\nerror: {message}\n```')


class AriaGlobalBans:
    def __init__(self, liara):
        self.liara = liara
        self.db = None  # gets populated later
        self.liara.loop.create_task(self.init())

        self.task = self.liara.loop.create_task(self.loop())

    def __unload(self):
        self.task.cancel()

    async def loop(self):
        while True:
            await self.liara.wait_until_ready()
            if not self.db:
                await asyncio.sleep(1)
                continue
            banned = await self.db.fetch('SELECT "user" FROM aria.global_bans')
            banned = [x['user'] for x in banned]
            for member in self.liara.get_all_members():
                if member.id not in banned:
                    await asyncio.sleep(0.001)
                    continue
                await self.process_ban(member)
            await asyncio.sleep(30)

    async def init(self):  # set up the database from the database cog
        cog = self.liara.get_cog('DB')
        await cog.ready.wait()
        self.db = cog.psql

    async def db_get_guild(self, guild_id) -> bool:
        r = await self.db.fetchval('SELECT global_bans FROM aria.guild_options WHERE guild = $1', guild_id)
        if not r:
            return False
        return True

    async def db_set_guild(self, guild_id, state):
        query = (
            'INSERT INTO aria.guild_options (guild, global_bans) VALUES ($1, $2) '
            'ON CONFLICT (guild) DO UPDATE SET global_bans = EXCLUDED.global_bans'
        )
        await self.db.execute(query, guild_id, state)

    async def db_get_ban(self, uid):
        return await self.db.fetchrow('SELECT * FROM aria.global_bans WHERE "user" = $1', uid)

    async def db_create_ban(self, uid, mid, reason, proof=None):
        query = 'INSERT INTO aria.global_bans ("user", moderator, reason, proof) VALUES ($1, $2, $3, $4)'
        return await self.db.execute(query, uid, mid, reason, proof)

    async def create_ban_reason(self, record: asyncpg.Record, guild):
        uid = record['user']
        mid = record['moderator']
        reason = record['reason']
        proof = record['proof']

        _proof = f' (with proof {proof})' if proof else ''

        _reason = f'Created for user with ID {uid}'
        appeals_guild = self.liara.get_guild(257454900311621642)
        invite = await appeals_guild.text_channels[0].create_invite(max_age=0, max_uses=1, reason=_reason, unique=False)

        moderator = self.liara.get_user(mid)

        embed = discord.Embed()
        embed.title = 'Global ban notice'
        embed.description = (
            f'You have been global banned by {moderator} ({mid}).\n'
            f'**Reason:** {reason}{_proof}\n'
            f'**Server you have been removed from:** {guild}\n'
            f'If you wish to appeal this ban, please click [here]({invite.url}).\n'
            f'Please note that this is the last chance you will get to redeem yourself.\n\n'
            f'*While this is not an offical ban from Discord, it is a good sign that you are on your way to getting '
            f'one.*'
        )
        return embed

    async def process_ban(self, member):
        state = await self.db_get_guild(member.guild.id)
        if not state:
            return  # it's disabled
        ban = await self.db_get_ban(member.id)
        if not ban:
            return  # they're not banned
        # uh oh
        # inform them
        embed = await self.create_ban_reason(ban, member.guild)
        message = None
        try:
            message = await member.send(embed=embed)
        except discord.HTTPException:
            pass  # oh well, we tried
        # ban them
        reason = 'Global ban'
        # softbanning for message cleanup, who knows what they've sent
        try:
            await member.ban(delete_message_days=7, reason=reason)
            await member.unban(reason=reason)
        except discord.Forbidden:  # must be staff, we got hierarchy'd (or simply have no permissions)
            if message:
                await message.delete()  # this will happen at worst once, so a ghost message is fine
            await self.db_set_guild(member.guild.id, False)

    async def on_member_join(self, member):
        if self.db is None:
            return
        await self.process_ban(member)

    @commands.command()
    @checks.is_owner()
    async def gban(self, ctx, *args):
        """Bans a user."""
        parser = DiscordFriendlyArgparse(prog=ctx.command.name, add_help=True)
        parser.add_argument('-u', '--users', nargs='+', type=int, metavar='ID', required=True,
                            help='users to global ban')
        parser.add_argument('-r', '--reason', help='a reason for the global ban')
        parser.add_argument('-p', '--proof', help='proof for the global ban')
        try:
            args = parser.parse_args(args)
        except DiscordArgparseError as e:
            return await ctx.send(str(e))

        if not args.reason and not args.proof:
            return await ctx.send('Please specify either a reason or proof.')

        for user in args.users:
            try:
                await self.db_create_ban(user, ctx.author.id, args.reason, args.proof)
            except asyncpg.UniqueViolationError:
                await ctx.send(f'User with ID {user} not banned, they were already.')

        m = 's' if len(args.users) != 1 else ''
        await ctx.send(f'User{m} banned with reason `{args.reason}` and proof `{args.proof}`.')
        self.liara.dispatch('stats_update')

    @commands.command()
    @commands.cooldown(1, 10, commands.BucketType.guild)
    async def gbaninfo(self, ctx, *user_ids: int):
        """Gets global ban info on users.

        - user_ids: A list of user IDs to get bans for
        """
        if not user_ids:
            return await self.liara.send_command_help(ctx)

        if len(user_ids) > 5:
            return await ctx.send('Please specify up to 5 users at a time.')

        # acquiring the get_user function from our modlog
        cog = self.liara.get_cog('AriaModlog')
        if cog is None:
            return await ctx.send('Unable to fetch users at this time.')
        get_user = cog.get_user

        # noinspection PyShadowingNames
        async def construct_embed(ban):
            embed = discord.Embed()
            embed.title = 'Global ban'

            embed.timestamp = ban['timestamp']

            user = await get_user(ban['user'])
            moderator = await get_user(ban['moderator'])
            embed.add_field(name='User', value=f'{user} ({ban["user"]})')
            embed.add_field(name='Moderator', value=f'{moderator} ({moderator.id})')  # moderators should always exist
            if ban['reason']:
                embed.add_field(name='Reason', value=ban['reason'], inline=False)
            if ban['proof']:
                embed.add_field(name='Proof', value=ban['proof'], inline=False)

            return embed

        ban_embeds = []
        unrecognized = []
        for _id in set(user_ids):
            ban = await self.db_get_ban(_id)
            if not ban:
                unrecognized.append(_id)
                continue
            ban_embeds.append(await construct_embed(ban))

        # sending the actual messages
        if unrecognized:
            await ctx.send('Unbanned users specified: {}'.format(','.join([f'`{x}`' for x in unrecognized])))
        for embed in ban_embeds:
            await ctx.send(embed=embed)
            await asyncio.sleep(1)

    @commands.command()
    @commands.guild_only()
    @checks.admin_or_permissions(ban_members=True)
    async def gbans(self, ctx, state: bool=None):
        """Toggles global bans for your guild.

        What are global bans? They're a list maintained by the moderators of
        this bot, preventing the worst of the worst from joining your server.
        This feature also seeks out global banned users currently on your
        server and will (soft)ban those too.

        - state: yes, no, on, off, you get the idea
        """
        if state is None:
            await self.liara.send_command_help(ctx)
            current = await self.db_get_guild(ctx.guild.id)
            if current:
                await ctx.send('Global bans are currently **on**.')
            else:
                await ctx.send('Global bans are currently **off**. Enabling them is probably a good idea.')
            return
        await self.db_set_guild(ctx.guild.id, state)
        state = 'on' if state else 'off'
        await ctx.send(f'Global bans turned **{state}**.')
