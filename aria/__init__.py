import importlib

from aria import welcome, global_bans, meta, modlog, filter, tempvoice

importlib.reload(welcome)
importlib.reload(global_bans)
importlib.reload(meta)
importlib.reload(modlog)
importlib.reload(filter)
importlib.reload(tempvoice)


def setup(liara):
    liara.add_cog(welcome.AriaWelcome(liara))
    liara.add_cog(global_bans.AriaGlobalBans(liara))
    liara.add_cog(meta.AriaMeta(liara))
    liara.add_cog(modlog.AriaModlog(liara))
    liara.add_cog(filter.AriaFilter(liara))
    liara.add_cog(tempvoice.AriaTempVoice(liara))
