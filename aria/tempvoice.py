import discord
import asyncio
from discord.ext import commands

from cogs.utils import checks
from cogs.utils.storage import RedisCollection


# noinspection PyIncorrectDocstring
class AriaTempVoice:
    def __init__(self, liara):
        self.liara = liara
        self.db = None
        self.tmp = RedisCollection(self.liara.redis, 'aria.tempvoice')
        self.liara.loop.create_task(self._init())
        self.tracked_channels = None

    def __unload(self):
        self.liara.loop.create_task(self.tmp.set('tracked', self.tracked_channels))

    async def _init(self):
        self.tracked_channels = await self.tmp.get('tracked', set())

        cog = self.liara.get_cog('DB')
        await cog.ready.wait()
        self.db = cog.psql

        # create
        await self.db.execute("""
        CREATE TABLE aria.tempvoice_lobbies
        (
            lobby BIGINT NOT NULL,
            guild BIGINT NOT NULL,
            override_grant_creator BIGINT DEFAULT 269484048 NOT NULL,
            override_grant_everyone BIGINT DEFAULT 0 NOT NULL,
            override_deny_everyone BIGINT DEFAULT 1048576 NOT NULL,
            name_template TEXT DEFAULT 'Temporary Channel: $name$' NOT NULL
        );
        CREATE UNIQUE INDEX tempvoice_lobbies_lobby_uindex ON aria.tempvoice_lobbies (lobby);
        """)

    async def on_voice_state_update(self, member, before, after):
        await asyncio.gather(self._process_channel(before), self._process_lobby(member, after))

    async def _process_channel(self, before):
        if not before.channel:
            return
        channel = self.liara.get_channel(before.channel.id)
        if not channel:
            return
        if channel.id not in self.tracked_channels:
            return
        if len(channel.members) == 0 and channel.guild.me.permissions_in(channel).manage_channels:
            await channel.delete()
            self.tracked_channels.remove(channel.id)

    async def _process_lobby(self, member, after):
        # event processing
        if not after.channel:
            return

        guild = member.guild
        if guild is None:
            return  # /shrug

        if not (guild.me.guild_permissions.manage_channels and guild.me.guild_permissions.move_members):
            return

        # lobby processing
        lobby = await self.db.fetchrow('SELECT * FROM aria.tempvoice_lobbies WHERE lobby = $1', after.channel.id)
        if lobby is None:
            return
        lobby_channel = after.channel

        # channel creation
        overwrites = {
            guild.default_role: discord.PermissionOverwrite.from_pair(
                discord.Permissions(lobby['override_grant_everyone']),
                discord.Permissions(lobby['override_deny_everyone'])
            ),
            member: discord.PermissionOverwrite.from_pair(
                discord.Permissions(lobby['override_grant_creator']),
                discord.Permissions.none()
            )
        }

        name = lobby['name_template'].replace('$name$', str(member))[0:100]
        channel = await guild.create_voice_channel(name, overwrites=overwrites, category=lobby_channel.category)
        await channel.edit(position=lobby_channel.position+1)
        self.tracked_channels.add(channel.id)
        await member.move_to(channel)

    @commands.command('designate-lobby', aliases=['designate_lobby'])
    @checks.admin_or_permissions(manage_channels=True)
    async def designate(self, ctx, *, channel: discord.VoiceChannel):
        """
        Sets up a voice channel as a lobby.
        :param channel: What channel to designate as lobby (when in doubt, use ID).
        """
        def check(_msg):
            if _msg.author == ctx.message.author and _msg.channel == ctx.message.channel and _msg.content:
                return True
            else:
                return False

        existing = await self.db.fetchrow('SELECT * FROM aria.tempvoice_lobbies WHERE lobby = $1', channel.id)
        if existing:
            return await ctx.send('Channel is already designated as a lobby.')

        await ctx.send(f'Setting up {channel} ({channel.id}) as a lobby channel. Is this okay? (y/n)')
        message = await self.liara.wait_for('message', check=check)
        if message.content.lower() not in ['y', 'yes', 'yep']:
            return await ctx.send('Lobby setup cancelled.')

        await self.db.execute('INSERT INTO aria.tempvoice_lobbies (lobby, guild) VALUES ($1, $2)',
                              channel.id, ctx.guild.id)

    @commands.command('remove-lobby', aliases=['remove_lobby'])
    @checks.admin_or_permissions(manage_channels=True)
    async def remove(self, ctx, *, channel: discord.VoiceChannel):
        """
        Removes a lobby.

        :param channel: The lobby to remove.
        """
        res = await self.db.execute('DELETE FROM aria.tempvoice_lobbies WHERE lobby = $1', channel.id)
        if res.split()[1] == '0':
            await ctx.send('Lobby not removed, it didn\'t exist.')
        else:
            await ctx.send('Lobby removed. Channels created as a result were not removed, they will expire over time.')

    @commands.command('set-lobby-perms', aliases=['set_lobby_perms', 'set-lobby-permissions', 'set_lobby_permissions'])
    @checks.admin_or_permissions(manage_channels=True)
    async def set_perms(self, ctx, channel: discord.VoiceChannel, creator_grants: int, everyone_grants: int,
                        everyone_denys: int):
        """
        Sets new subchannel permissions.

        :param channel: The lobby to modify.
        :param creator_grants: What permissions to grant to the channel creator in a newly-created channel.
        :param everyone_grants: What permissions to grant to at-everyone in a newly-created channel.
        :param everyone_denys: What permissions to deny from at-everyone in a newly-created channel.

        The expected type for all permission parameters is a permission number.
        If you don't know how to generate these, use https://finitereality.github.io/permissions
        """
        q = """
        UPDATE aria.tempvoice_lobbies SET
        override_grant_creator = $1,
        override_grant_everyone = $2,
        override_deny_everyone = $3
        WHERE lobby = $4
        """
        res = await self.db.execute(q, creator_grants, everyone_grants, everyone_denys, channel.id)
        if res.split()[1] == '0':
            await ctx.send('Lobby not updated, it didn\'t exist.')
        else:
            await ctx.send('Lobby updated.')

    @commands.command('set-lobby-text', aliases=['set_lobby_text'])
    @checks.admin_or_permissions(manage_channels=True)
    async def set_text(self, ctx, channel: discord.VoiceChannel, *, text):
        """
        Set new subchannel default name.

        :param channel: The lobby to modify.
        :param text: The new default name. Use $name$ for the user's name.
        """
        q = """
        UPDATE aria.tempvoice_lobbies SET
        name_template = $1
        WHERE lobby = $2
        """
        res = await self.db.execute(q, text, channel.id)
        if res.split()[1] == '0':
            await ctx.send('Lobby not updated, it didn\'t exist.')
        else:
            await ctx.send('Lobby updated.')
